if io.open(os.getenv("HOME") .. "/.config/vis/plugins/vis-plug",r) == nil then 
    os.execute("curl https://raw.githubusercontent.com/erf/vis-plug/master/init.lua -o $HOME/.config/vis/plugins/vis-plug/init.lua --create-dirs")
    vis:command("plug-install")
end

require('vis')
local plugins = {
	['https://github.com/erf/vis-cursors.git']         = 'init',
	['https://github.com/lutobler/vis-commentary.git'] = 'vis-commentary',
	['https://github.com/erf/vis-title.git'] = 'init',
	['https://github.com/lutobler/vis-modelines.git'] = 'vis-modelines',
	['https://github.com/jocap/vis-filetype-settings.git']='vis-filetype-settings',
	['https://gitlab.com/timoha/vis-go.git']='vis-go',
	['https://github.com/roguh/vis-backup.git']='backup',
	['https://github.com/kupospelov/vis-ctags.git']='ctags',
	['https://github.com/ingolemo/vis-smart-backspace.git']='init',
	['https://gitlab.com/szanko/vis-markdown-preview.git']='init',
	['https://localhost/szanko/test.git']='init',
}
require('plugins/vis-plug').init(plugins)

settings = {
    java = { 'set autoindent', 'set expandtab', 'set tabwidth 4' },
    yaml = { 'set autoindent', 'set expandtab', 'set tabwidth 2' },
    lua  = { 'set autoindent', 'set expandtab', 'set tabwidth 4' },
    py   = { 'set autoindent', 'set expandtab', 'set tabwidth 4' },
    python   = { 'set autoindent', 'set expandtab', 'set tabwidth 4' },
    dart = {'set autoindent','set expandtab','set tabwidth 4'},
}
vis.events.subscribe(vis.events.INIT, function()
    background="dark"
    --vis:command('set theme peaksea/peaksea')
    --vis:command('set theme gruvbox')
end)

vis.events.subscribe(vis.events.WIN_OPEN, function(win)
    vis:command('set cursorline')
    vis:command('set number')
    --Map Capslock Key to Escape
    vis:command("!xmodmap -e 'clear Lock' -e 'keycode 0x42 = Escape'")
end)

vis.events.subscribe(vis.events.WIN_CLOSE, function(win)
    --Resets the Capslock Key
    --vis:command("!xmodmap -e 'clear Lock' -e 'keycode 0x42 = Caps_Lock'")
end)
