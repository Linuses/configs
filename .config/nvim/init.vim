let vimplug_exists=expand('~/.config/nvim/autoload/plug.vim')
if has('win32')&&!has('win64')
  let curl_exists=expand('C:\Windows\Sysnative\curl.exe')
else
  let curl_exists=expand('curl')
endif
if !filereadable(vimplug_exists)
  if !executable(curl_exists)
    echoerr "You have to install curl or first install vim-plug yourself!"
    execute "q!"
  endif
  echo "Installing Vim-Plug..."                                              
  echo ""                                                                    
  silent exec "!"curl_exists" -fLo " . shellescape(vimplug_exists) . " --crea    te-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" 
  let g:not_finish_vimplug = "yes"                                           
                                                                             
  autocmd VimEnter * PlugInstall                                             
endif                                                                        
                                                                             
" Required:                                                                  
call plug#begin(expand('~/.config/nvim/plugged'))                            
Plug 'tpope/vim-surround'
Plug 'preservim/nerdtree'
Plug 'jreybert/vimagit'
Plug 'bling/vim-airline'
Plug 'tpope/vim-commentary'
Plug 'itchyny/lightline.vim'
Plug 'xuhdev/vim-latex-live-preview', { 'for': 'tex' }
Plug 'Yggdroot/indentLine'
Plug 'sheerun/vim-polyglot'
Plug 'rhysd/vim-grammarous'
Plug 'luochen1990/rainbow', {'for':['typescript','dart','tex']}
" Plug 'lervag/vimtex', { 'for': 'tex' }
Plug 'dense-analysis/ale'
Plug 'flazz/vim-colorschemes'
Plug 'vim-scripts/peaksea'
"Plug 'ycm-core/lsp-examples'
Plug 'dart-lang/dart-vim-plugin', {'for':'dart'}
Plug 'thosakwe/vim-flutter'
Plug 'neoclide/coc.nvim', {'branch':'release'}
",'for':'tex'}
Plug 'iamcco/markdown-preview.nvim' ,{'do': 'cd app & yarn install','for':'markdown'}
call plug#end()

set number
set encoding=utf-8
syntax on
setlocal spell
set spelllang=de,en_gb
set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab
set title 
if ! has("gui_running")
    set t_Co=256
endif 
set bg=dark
colors gruvbox

vnoremap <C-y> "+y
map <C-p> "+p

nnoremap <A-h> <C-w>h
nnoremap <A-j> <C-w>j
nnoremap <A-k> <C-w>k
nnoremap <A-l> <C-w>l

noremap <silent> <C-h> :vertical resize -3 <CR>
noremap <silent> <S-C-j> :resize +3 <CR>
noremap <silent> <S-C-k> :resize -3 <CR>
noremap <silent> <C-l> :vertical resize +3 <CR>

nnoremap ,html :-1read $HOME/Vorlagen/programmieren/web/html/basictemplate.html<CR>4jwf>a
nnoremap ,py :-1read $HOME/Vorlagen/programmieren/python/basicpython.py<CR>4jwf>a
nnoremap ,beg :-1read $HOME/Vorlagen/latex/begin_end.tex<CR>2wei
nnoremap ,java :-1read $HOME/Vorlagen/programmieren/java/basic_class.java<CR>

nnoremap <A-n> :NERDTreeToggle<CR>
nnoremap ,, <Esc>/<++><Enter>"_c4l
inoremap [ []<Esc>i
inoremap { {}<Esc>i
inoremap ( ()<Esc>i
inoremap " ""<Esc>i
inoremap ' ''<Esc>i
inoremap < <><Esc>i

let g:livepreview_previewer = 'zathura'
let g:livepreview_use_biber = 1
let g:livepreview_cursorhold_recompile = 0
let g:rainbow_active = 1
let g:ale_completion_autoimport = 1
let g:ale_fix_on_save = 1
let g:lsc_auto_map= v:true
let g:lsc_server_commands = {'dart': 'dart_language_server'}

fun! GoYCM()
    let g:ycm_global_ycm_extra_conf = '~/.config/nvim/plugged/YouCompleteMe/third_party/ycmd/.ycm_extra_conf.py'
	nnoremap <buffer> <silent> <leader> 1f :YcmCompleter GoTo<CR>
	nnoremap <buffer> <silent> <leader> 2f :YcmCompleter GoToReferences<CR>
	nnoremap <buffer> <silent> <leader> 3f :YcmCompleter RefactorRename<CR>
endfun

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

fun! GoCoc()
	inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
	inoremap <buffer> <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"
	inoremap <buffer> <silent><expr> <C-space> coc#refresh()

	" GoTo code navigation.
	nmap <silent> 1f <Plug>(coc-definition)
	nmap <silent> 4f <Plug>(coc-type-definition)
	nmap <silent> 3f <Plug>(coc-implementation)
	nmap <silent> 2f <Plug>(coc-references)
	

endfun

fun! CurlyBrackets()
	inoremap { {<CR>}<Esc>ko
endfun

fun! Java()
     let $JAVA_TOOL_OPTIONS='-javaagent:/usr/lib/lombok-common/lombok.jar'
endfun

fun! Html()
	nnoremap <F5> :silent update<Bar>silent !firefox %:p &<CR>
	inoremap !comment <!-- <++> --><Esc>
	inoremap !DOC <!DOCTYPE html><Esc>
	inoremap !div <div > <++> </div><Esc>02ei
	"nnoremap <F5> :silent update<Bar>silent !firefox %:p:s?\(.\{-}/\)\{4}?http://localhost/?<CR>
endfun


fun! Latex()
	nnoremap <buffer> <F5> :LLPStartPreview<CR>
endfun

autocmd FileType tex :call Latex()
"autocmd FileType dart,cpp,cxx,h,hpp,c :call GoCoc()
autocmd FileType java :call Java()
autocmd FileType typescript,java,text,nvim,vim,markdown,python,jproperties,cpp,cxx,h,c,hpp :call GoYCM()
autocmd FileType html :call Html()
autocmd FileType dart,cpp,cxx,h,hpp,c,java :call CurlyBrackets()
au VimEnter * silent !xmodmap -e 'clear Lock' -e 'keycode 0x42 = Escape'
"au VimLeave * silent !xmodmap -e 'clear Lock' -e 'keycode 0x42 = Caps_Lock'

let s:lsp = '~/.config/nvim/plugged/lsp-examples'


